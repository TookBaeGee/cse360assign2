/* Author: Jason Kwon
 * Class ID: 447
 * Assignment 2
 * Description: The assignment 2 is to learn how to manage efficient version control and use git (github or bitbucket) to commit changes to the original versions.
 */

/*
 * Class Calculator has methods that do simple arithmatics to the total, print the total, and print the history.
 */

public class Calculator {

	private int total;
	private static String history;

	//basic constructor
	public Calculator(){
		total = 0;  // not needed - included for clarity
		history = "0";
	}
	
	//prints total
	public int getTotal () {
		return total;
	}

	//adds value to total
	public void add (int value) {
		total += value;
		history = history + " + " + value;
	}
	
	//subtracts value to total
	public void subtract (int value) {
		total -= value;
		history = history + " - " + value;
	}
	
	//multiplies value to total
	public void multiply (int value) {
		total *= value;
		history = history + " * " + value;
	}
	
	//divides value to total
	public void divide (int value) {
		if(value == 0) {
			total = 0;
			history = "0";
		}
		else {
			total /= value;
			history = history + " / " + value;
		}
	}
	
	//prints the history of the arithmatics done to total.
	public String getHistory () {
		return history;
	}
}